#include <opencv2/highgui/highgui.hpp>
#include <boost/filesystem.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <iostream>
#include <algorithm>
using namespace boost::filesystem;
using namespace boost::numeric::ublas;
using namespace cv;
using namespace std;

struct Region {
	int cnt,x, y;
};
bool sortRegions(const Region& first, const Region& second)
{
	return first.cnt < second.cnt;
}
static int selColor = 5;
static int checkingValue = 255;
//colors [0] = red, [1] = green, [2] = blue, [3] = yellow, [4] = magenta, [5] = cyan
static Vec3b colors[] = { { 0, 0, 255 }, { 0, 255, 0 }, { 255, 0, 0 }, { 0, 255, 255 }, { 255, 0, 255 }, { 255, 255, 0 } };
void reset()
{
	selColor = 5;
	checkingValue = 255;
}
//recursivley searches for region. Starts by changing all pixels to right, then left, then down, then up, then top right, then top left,
//then bottom right, then bottom left for each pixel
int colorizeRegion(Mat &image, Mat &colorImage, int x, int y, int pc = 0, bool changeColor = false)
{
	if (changeColor)
	{
		checkingValue = 254;
	}
	//sets color -> since function is recursive its original value is 5 and when coming from main it is changed b/c main doesn't set 
	//pixelCount and therefor pixelCount is 0 only when coming from main. Since the original value is
	if (pc == 0)
		selColor < 5 ? selColor++ : selColor = 0;
	if (x > 0 && y > 0 && y < image.rows && x < image.cols && image.at<uchar>(y, x) == checkingValue)
	{
		if (changeColor)
			colorImage.at<Vec3b>(y, x) = colors[selColor];
		image.at<uchar>(y, x) = checkingValue - 1;
		//checking right
		pc = colorizeRegion(image, colorImage, x + 1, y, pc + 1, changeColor);
		//checking left
		pc = colorizeRegion(image, colorImage, x - 1, y, pc + 1, changeColor);
		//checking down
		pc = colorizeRegion(image, colorImage, x, y + 1, pc + 1, changeColor);
		//checking up
		pc = colorizeRegion(image, colorImage, x, y - 1, pc + 1, changeColor);
		//checking top right
		pc = colorizeRegion(image, colorImage, x + 1, y - 1, pc + 1, changeColor);
		//checking top left
		pc = colorizeRegion(image, colorImage, x - 1, y - 1, pc + 1, changeColor);
		//checking bottom right
		pc = colorizeRegion(image, colorImage, x + 1, y + 1, pc + 1, changeColor);
		//checking bottom left
		pc = colorizeRegion(image, colorImage, x - 1, y + 1, pc + 1, changeColor);
	}
	else
		pc--;
	return pc;
}

int main(int argc, char **argv)
{
	// Grab first command line parameter as path
	path p(argv[1]);
	// Create "end-of-list" iterator
	directory_iterator end_itr;
	int i, j, T = 130, pixelCount;//counter,counter,threshold,Total#OfRegions,PixelCountForSpecificRegion
	int rowCnt;
	int colCnt;
	string OutputURL = "";
	// Holds images
	Mat image;
	Mat colorImage;
	Region curReg;
	list<Region> Regions;
	for (directory_iterator itr(p); itr != end_itr; ++itr)
	{
		if (itr->path().extension() == ".bmp")
		{
			// Read the file
			image = imread(itr->path().string(), IMREAD_GRAYSCALE);
			// Check if data is invalid
			if (!image.data)
			{
				cout << "ERROR: Could not open or find the image!" << endl;
				return -1;
			}
			rowCnt = image.rows;
			colCnt = image.cols;
			colorImage = Mat::zeros(cv::Size(colCnt, rowCnt), CV_8UC3);
			//Threshold image
			for (i = 0; i<image.rows; i++)
			{
				for (j = 0; j<image.cols; j++)
				{
					if (image.at<uchar>(i, j) >= T)
						image.at<uchar>(i, j) = 255;
					else
						image.at<uchar>(i, j) = 0;
				}
			}
			//create Output URL, Ex: c:\Name\Location\image.bmp
			OutputURL = argv[2];
			OutputURL += '\\';
			OutputURL += "BIN_";
			OutputURL += itr->path().filename().string();
			//save file
			imwrite(OutputURL, image);
			//FloodFilling, if coordinate == 1, then its value after thresholding was 0, but is now 1 b/c we visited it.
			//If coordinate == 249 then its value after thresholding was 250, but is now 249 b/c we visited it.
			for (i = 0; i<image.rows; i++)
			{
				for (j = 0; j<image.cols; j++)
				{
					if (image.at<uchar>(i,j) == 255)
					{
						curReg.cnt = colorizeRegion(image, colorImage, j, i)+1;
						curReg.x = j;
						curReg.y = i;
						if (curReg.cnt > 0)
							Regions.push_back(curReg);
					}
				}
			}
			selColor = 5;
			//create Output URL, Ex: c:\Name\Location\image.bmp
			OutputURL = argv[2];
			OutputURL += '\\';
			OutputURL += itr->path().filename().string();
			OutputURL += ".txt";
			//Save file
			ofstream outputFile(OutputURL);
			outputFile << Regions.size() << "\n";
			Regions.sort(sortRegions);
			while (!Regions.empty())
			{
				curReg = Regions.back();
				Regions.pop_back();
				outputFile << curReg.cnt << "\n";
				colorizeRegion(image, colorImage, curReg.x, curReg.y, 0, true);
			}
			outputFile.close();
			//create Output URL, Ex: c:\Name\Location\image.bmp
			OutputURL = argv[2];
			OutputURL += '\\';
			OutputURL += "SEG_";
			OutputURL += itr->path().filename().string();
			//save file
			imwrite(OutputURL, colorImage);
			reset();
		}
	}
	cout << "Finished floodfilling successfully.\n";
	return 0;
}